# Copyright 2014, 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=Version-${PV} pnv=SuperCollider-${PV}-Source suffix=tar.bz2 ]
require cmake
require freedesktop-desktop freedesktop-mime gtk-icon-cache

SUMMARY="Language for real time audio synthesis and algorithmic composition"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    doc
    emacs
    qt
    vim

    amd64_cpu_features: sse sse2
"

# 78% tests passed, 4 tests failed out of 18
RESTRICT="test"

QT_MIN_VER=5.7

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/boost[>=1.53.0]
        dev-libs/yaml-cpp:=
        media-libs/libsndfile
        media-sound/jack-audio-connection-kit
        sci-libs/fftw[>=3.3]
        sys-libs/readline:=[>=5.0]
        sys-sound/alsa-lib
        x11-libs/libX11
        x11-libs/libXt
        emacs? ( app-editors/emacs )
        qt? (
            x11-libs/qtbase:5[>=${QT_MIN_VER}][gui][sql]
            x11-libs/qtsvg:5[>=${QT_MIN_VER}]
            x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        )
        vim? (
            || ( app-editors/vim[ruby] )
            ( app-editors/gvim[ruby] )
        )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DFFT_GREEN:BOOL=FALSE
    -DFINAL_BUILD:BOOL=FALSE
    -DFORTIFY:BOOL=FALSE
    -DGC_SANITYCHECK:BOOL=FALSE
    -DHIDAPI:STRING=default
    -DHID_DEBUG_PASER:BOOL=FALSE
    -DHID_EXAMPLE_OSC:BOOL=FALSE
    -DHID_EXAMPLE_TEST:BOOL=FALSE
    -DHID_INSTALL_HUG:BOOL=TRUE
    -DLIBSCSYNTH:BOOL=TRUE
    -DLTO:BOOL=FALSE
    -DNATIVE:BOOL=FALSE
    -DNOVA_SIMD:BOOL=TRUE
    -DNO_AVAHI:BOOL=TRUE
    -DNO_GPL3:BOOL=FALSE
    -DNO_LIBSNDFILE:BOOL=FALSE
    -DNO_X11:BOOL=FALSE
    -DSCLANG_SERVER:BOOL=TRUE
    -DSC_ABLETON_LINK:BOOL=FALSE
    -DSC_CLANG_USES_LIBSTDCPP:BOOL=FALSE
    -DSC_DOC_RENDER:BOOL=FALSE
    -DSC_ED:BOOL=FALSE
    -DSC_HIDAPI:BOOL=FALSE
    -DSC_PLUGIN_DIR:PATH=/usr/$(exhost --target)/lib/SuperCollider/plugins
    -DSC_SYMLINK_CLASSLIB:BOOL=FALSE
    -DSN_MEMORY_DEBUGGING:BOOL=FALSE
    -DSUPERNOVA:BOOL=TRUE
    -DSYSTEM_ABLETON_LINK:BOOL=FALSE
    -DSYSTEM_BOOST:BOOL=TRUE
    -DSYSTEM_YAMLCPP:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'amd64_cpu_features:sse SSE'
    'amd64_cpu_features:sse2 SSE2'
    'doc INSTALL_HELP'
    'debug SC_MEMORY_DEBUGGING'
    'emacs SC_EL'
    'qt SC_IDE'
    'qt SC_QT'
    'qt SC_USE_QTWEBENGINE'
    'vim SC_VIM'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    '-DENABLE_TESTSUITE:BOOL=TRUE -DENABLE_TESTSUITE:BOOL=FALSE'
)

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.11.2-no-ccache.patch
    "${FILES}"/${PN}-3.11.2-musl-implement-correct-xsi.patch
    "${FILES}"/b9dd70c4c8d61c93d7a70645e0bd18fa76e6834e.patch
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( README.txt README_LINUX.txt )

src_install() {
    cmake_src_install

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share/SuperCollider/SCClassLibrary/External

    # TODO: fix upstream
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/{icons,mime} "${IMAGE}"/usr/share/
    option qt && edo mv "${IMAGE}"/usr/$(exhost --target)/share/applications "${IMAGE}"/usr/share/
}

pkg_postinst() {
    option qt && freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    option qt && freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

