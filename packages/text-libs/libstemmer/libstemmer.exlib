# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=snowballstem pn=snowball tag=v${PV} ]

export_exlib_phases src_fetch_extra src_unpack src_test src_install

SUMMARY="Snowball stemming algorithms as a C library"
DESCRIPTION="
Snowball is a small string processing language for creating stemming
algorithms for use in Information Retrieval, plus a collection of stemming
algorithms implemented using it.
The Snowball compiler translates a Snowball program into source code in
another language - currently Ada, ISO C, C#, Go, Java, Javascript, Object
Pascal, Python and Rust are supported."

HOMEPAGE="https://snowballstem.org/ ${HOMEPAGE}"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

WORK="${WORKBASE}"/snowball-${PV}

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-shared-library.patch
)

_TEST_DATA_COMMIT="0703f1d6a21802c3ff00c2c8b31bd255b74b2aec"

libstemmer_src_fetch_extra() {
    if expecting_tests && [[ ! -e "${FETCHEDDIR}"/snowball-data-${_TEST_DATA_COMMIT}.tar.gz ]] ; then
        edo wget -O "${FETCHEDDIR}"/snowball-data-${_TEST_DATA_COMMIT}.tar.gz \
            https://github.com/snowballstem/snowball-data/archive/${_TEST_DATA_COMMIT}.tar.gz
    fi
}

libstemmer_src_unpack() {
    default

    if expecting_tests ; then
        edo tar xzf "${FETCHEDDIR}"/snowball-data-${_TEST_DATA_COMMIT}.tar.gz
    fi
}

libstemmer_src_test() {
    emake -j1 STEMMING_DATA="${WORKBASE}/snowball-data-${_TEST_DATA_COMMIT}" check
}

libstemmer_src_install() {
    dobin stemwords

    insinto /usr/$(exhost --target)/include
    doins include/${PN}.h

    dolib.so ${PN}.so.0.0.0
    dosym ${PN}.so.0.0.0 /usr/$(exhost --target)/lib/${PN}.so.0
    dosym ${PN}.so.0 /usr/$(exhost --target)/lib/${PN}.so

    emagicdocs
}

