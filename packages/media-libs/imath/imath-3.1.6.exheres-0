# Copyright 2021-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=AcademySoftwareFoundation project=${PN^} tag=v${PV} ] \
    cmake

SUMMARY="Library of 2D and 3D vector, matrix, and math operations for computer graphics"
HOMEPAGE+=" https://www.openexr.com"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !media-libs/ilmbase [[
            description = [ Imath was formerly distributed via IlmBase ]
            resolution = uninstall-blocked-after
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DDOCS:BOOL=FALSE
    -DIMATH_ENABLE_LARGE_STACK:BOOL=FALSE
    -DIMATH_HALF_USE_LOOKUP_TABLE:BOOL=TRUE
    -DIMATH_INSTALL_PKG_CONFIG:BOOL=TRUE
    -DIMATH_INSTALL_SYM_LINK:BOOL=TRUE
    -DIMATH_USE_CLANG_TIDY:BOOL=FALSE
    -DIMATH_USE_DEFAULT_VISIBILITY:BOOL=FALSE
    -DIMATH_USE_NOEXCEPT:BOOL=TRUE
    -DPYTHON:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

