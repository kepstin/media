Source: https://github.com/sergiomb2/libmp4v2/commit/f5f814801ecd312a1418e2226dadfea72badec49
Upstream: Unknown
Reason: CVE-2018-14325, CVE-2018-14326, CVE-2018-14446

From f5f814801ecd312a1418e2226dadfea72badec49 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?S=C3=A9rgio=20M=2E=20Basto?= <sergio@serjux.com>
Date: Sat, 2 Nov 2019 04:37:15 +0000
Subject: [PATCH] Fix v2 Integer underflow/overflow in MP4v2 2.0.0

Reference: https://www.openwall.com/lists/oss-security/2018/07/16/1

For the overflow, we could check the result of the integer multiplication:

Addresses https://nvd.nist.gov/vuln/detail/CVE-2018-14326 and https://nvd.nist.gov/vuln/detail/CVE-2018-14446

For the underflow, we could check if `dataSize >= hdrSize` satisfies:
Throw exception when invalid atom size would cause integer underflow
The calculation `hdrSize - dataSize` can underflow the 64-bit unsigned int dataSize type, which can lead to incorrect results.  We throw an exception to stop the code from going any further.

Addresses https://nvd.nist.gov/vuln/detail/CVE-2018-14325
Based on https://github.com/TechSmith/mp4v2/commit/e475013c6ef78093055a02b0d035eda0f9f01451
---
 src/mp4array.h  | 5 ++++-
 src/mp4atom.cpp | 6 ++++++
 2 files changed, 10 insertions(+), 1 deletion(-)

diff --git a/src/mp4array.h b/src/mp4array.h
index c49d59b..340d74d 100644
--- a/src/mp4array.h
+++ b/src/mp4array.h
@@ -102,8 +102,11 @@ class MP4Array {
         void Resize(MP4ArrayIndex newSize) { \
             m_numElements = newSize; \
             m_maxNumElements = newSize; \
+            uint32_t mul = newSize * sizeof(type); \
+            if(mul / newSize != sizeof(type)) \
+                throw new Exception("multiplication overflow", __FILE__, __LINE__, __FUNCTION__);\
             m_elements = (type*)MP4Realloc(m_elements, \
-                m_maxNumElements * sizeof(type)); \
+                mul); \
         } \
         \
         type& operator[](MP4ArrayIndex index) { \
diff --git a/src/mp4atom.cpp b/src/mp4atom.cpp
index 7a0a53f..f5d5dc0 100644
--- a/src/mp4atom.cpp
+++ b/src/mp4atom.cpp
@@ -143,6 +143,12 @@ MP4Atom* MP4Atom::ReadAtom(MP4File& file, MP4Atom* pParentAtom)
         dataSize = file.GetSize() - pos;
     }
 
+    if(dataSize < hdrSize) {
+        ostringstream oss;
+        oss << "Invalid atom size in '" << type << "' atom, dataSize = " << dataSize << " cannot be less than hdrSize = " << static_cast<unsigned>( hdrSize );
+        log.errorf( "%s: \"%s\": %s", __FUNCTION__, file.GetFilename().c_str(), oss.str().c_str() );
+        throw new Exception( oss.str().c_str(), __FILE__, __LINE__, __FUNCTION__ );
+    }
     dataSize -= hdrSize;
 
     log.verbose1f("\"%s\": type = \"%s\" data-size = %" PRIu64 " (0x%" PRIx64 ") hdr %u",
