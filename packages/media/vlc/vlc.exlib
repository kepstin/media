# Copyright 2008-2012 Daniel Mierswa <impulze@impulze.org>
# Copyright 2014-2016 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache flag-o-matic \
    lua [ multibuild=false with_opt=true whitelist="5.1 5.2" ] \
    ffmpeg [ options="[vdpau?]" with_opt=true ] \
    option-renames [ renames=[ 'secret keyring' ] ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst pkg_postrm

SUMMARY="VLC media player is a highly portable multimedia player, encoder and streamer"
DESCRIPTION="
VLC media player is a highly portable multimedia player for various audio and video formats as well
as DVDs, VCDs, and various streaming protocols.
"
HOMEPAGE="https://www.videolan.org/vlc"
DOWNLOADS="mirror://videolan/${PN}/${PV}/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="https://www.videolan.org/news.html"

LICENCES="
    GPL-2    [[ note = [ VLC media player ] ]]
    LGPL-2.1 [[ note = [ libVLC ] ]]
"
SLOT="0"

# FIXME:
# provide --with-a52-fixed if a52dec was built with fixed point support
# automagic linking of -lminizip (if found via pkg-config) if zlib.h is found
# automagic linking of -lgpg-error (always) if gcrypt is enabled (RAOP and HTTP filter)
# regularly check Modules.am and Makefile.am for -l* stuff in LIBADD
# also check for addition of X*LIBS and X*CFLAGS in configure and Makefiles

MYOPTIONS="
    a52             [[ description = [ Support ATSC A/52 (AC-3) decoding ] ]]
    aac             [[ description = [ Support AAC decoding ] ]]
    archive         [[ description = [ Support for unseekable access to archive files (rar, lha, tar, ...) ] ]]
    ass             [[ description = [ Support decoding (rendering) subtitles ] ]]
    av1             [[ description = [ Support decoding AV1 using dav1d ] ]]
    avahi           [[ description = [ Support Bonjour service discovery with avahi ] ]]
    bidi            [[ description = [ Support for bidirectional text ] ]]
    bluray          [[ description = [ Support accessing Blu-ray media with libbluray ] ]]
    caca            [[ description = [ Support colored ASCII video output ] ]]
    cddb            [[ description = [ Support fetching track information from CDDB when playing Audio CDs ] ]]
    chromecast      [[
                       description = [ Support for Chromecast streaming ]
                       requires = [ gnutls ]
    ]]
    crypt           [[ description = [ Support SRTP in the RTP access and stream output module, accessing a OSD over VNC ] ]]
    dc1394          [[ description = [ Support accessing/demuxing streams from IIDC cameras ] ]]
    dirac           [[ description = [ Support decoding dirac using the high performance implementation libschroedinger. ] ]]
    dts             [[ description = [ Support DTS Coherent Acoustics decoding ] ]]
    dv              [[ description = [ Support accessing Digital Video (Format used by some Camcorders) ] ]]
    dvb             [[ description = [ Support muxing/demuxing the MPEG Transport Stream and accessing DVB cards ] ]]
    dvd             [[ description = [ Support accessing/demuxing DVD media with libdvdnav and libdvdread ] ]]
    ffmpeg
    flac            [[ description = [ Support encoding/decoding/packetizing FLAC ] ]]
    fluidsynth      [[ description = [ Support MIDI synthesizing with fluidsynth ] ]]
    gnutls          [[ description = [ Support TLS (for example in the http access and httpd module) ] ]]
    gstreamer       [[ description = [ Video decoding via gstreamer ] ]]
    h264            [[ description = [ Support encoding H.264/MPEG4 AVC ] ]]
    hevc            [[ description = [ Support for HEVC/H.265 encoding using x265 ] ]]
    id3             [[ description = [ Support reading ID3v1/2 and APEv1/2 metadata with id3tag ] ]]
    jack            [[ description = [ Support JACK audio output and capturing ] ]]
    keyring         [[ description = [ Support for using libsecret for keystore ] ]]
    kwallet         [[ description = [ Support for using kwallet (via D-Bus) for keystore ] ]]
    libnotify       [[ description = [ Support playlist item changes through libnotify ] ]]
    libsamplerate   [[ description = [ Provide a resampling filter ] ]]
    linsys          [[ description = [ Support input from SDI / HD-SDI cards with zvbi ] ]]
    live            [[ description = [ Support accessing/demuxing RTSP/RTP/SDP with LiveMedia ] ]]
    lua             [[ description = [ Lua playlist, metafetcher and interface plugins ] ]]
    matroska        [[ description = [ Support demuxing Matroska streams ] ]]
    mp2             [[ description = [ Support MPEG-1 & 2 audio layer II encoding with libtwolame ] ]]
    mp3             [[ description = [ Support MPEG-1 & 2 audio layer I, II, III and MPEG 2.5 decoding with libmad ] ]]
    mpc             [[ description = [ Support demuxing MusePack streams ] ]]
    mpeg2           [[ description = [ Support decoding MPEG I/II video with libmpeg2 ] ]]
    mtp             [[ description = [ Support accessing and discovering of MTP devices ] ]]
    nfs             [[ description = [ Read and browse nfs filesystems (libnfs) ] ]]
    ogg             [[ description = [ Support muxing/demuxing ogg streams ] ]]
    opus            [[ description = [ Support decoding the opus codec ] ]]
    oss             [[ description = [ Provides an alternative sound architecture instead of ALSA ] ]]
    pulseaudio      [[ description = [ Support pulseaudio audio output ] ]]
    qt5             [[ description = [ Add a Qt5 GUI ] ]]
    samba           [[ description = [ Support accessing media on samba shares ] ]]
    sftp            [[ description = [ Support accessing media on SFTP servers ] ]]
    shout           [[ description = [ Support streaming mp3/ogg to shoutcast/icecast servers ] ]]
    skins           [[
                       description = [ Add a skinnable GUI ]
                       requires = [ qt5 ]
    ]]
    sndio           [[ description = [ Adds support for sound output through sndio
                                       (OpenBSD sound API, also ported to Linux) ] ]]
    soxr            [[ description = [ Resampler module based on SoX ] ]]
    speex           [[ description = [ Support encoding/decoding/packetizing Speex ] ]]
    svg             [[ description = [ Support rendering SVG in videos ] ]]
    taglib          [[ description = [ Support reading/writing metadata with taglib ] ]]
    teletext        [[ description = [ Support decoding VBI and Teletext PES with zvbi ] ]]
    theora          [[ description = [ Support encoding/decoding/packetizing Theora ] ]]
    truetype        [[ description = [ Support rendering text in videos ] ]]
    upnp            [[ description = [ Support Service Discovery via UPnP ] ]]
    v4l             [[ description = [ Support using v4l2 when accessing Video4Linux devices ] ]]
    va              [[ description = [ Accelerate ffmpeg decoding by using the Video Acceleration API ] ]]
    vdpau           [[ description = [ Enable the Video Display and Presentation API for Unix which offloads parts of video decoding to your GPU ] ]]
    vorbis          [[ description = [ Support encoding/decoding/packetizing Vorbis ] ]]
    vpx             [[ description = [ VP8/VP9 encoder (vlc-3.0 or higher) and decoder ] ]]
    wayland         [[ description = [ Support for video output on wayland display servers ] ]]
    x262            [[ description = [ Support for a MPEG-2 encoder based on x264 ] ]]

    ( bidi fontconfig skins ) [[ requires = truetype ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ogg             [[ requires = vorbis ]]
    va              [[ requires = ffmpeg ]]
    vdpau           [[ requires = ffmpeg ]]
"

# check_POTFILES.sh fails when building without Qt support
RESTRICT="test"

# We could keep requiring different versions of ffmpeg and libav depending
# on vdpau support, but I don't see the point...
DEPENDENCIES+="
    build:
        sys-devel/bison
        sys-devel/flex
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        dev-libs/libxml2:2.0[>=2.5]
        media-libs/libpng:=[>=1.5.4]
        net-dns/libidn
        sys-apps/dbus[>=1.6.0]
        sys-libs/ncurses[>=5.9-r2]
        sys-libs/zlib
        sys-sound/alsa-lib[>=1.0.16]
        x11-libs/libxcb[>=1.6]
        x11-libs/libX11
        x11-utils/xcb-util-keysyms[>=0.3.4]
        a52? ( media-libs/a52dec[>=0.7.3] )
        aac? ( media-libs/faad2 )
        archive? ( app-arch/libarchive[>=3.1.0] )
        ass? (
            media-libs/libass[>=0.9.8]
            media-libs/fontconfig
        )
        av1? ( media-libs/dav1d )
        avahi? (
            net-dns/avahi[>=0.6][dbus]
            [[ note = [ dbus option enables avahi-client ] ]]
        )
        bidi? ( dev-libs/fribidi )
        bluray? ( media-libs/libbluray[>=0.6.2] )
        caca? ( media-libs/libcaca[>=0.99_beta14] )
        cddb? ( media-libs/libcddb[>=0.9.5] )
        chromecast? (
            dev-libs/protobuf:=[>=2.5.0]
            net-libs/libmicrodns[>=0.1.2]
        )
        crypt? (
            dev-libs/libgcrypt[>=1.6.0]
            dev-libs/libgpg-error
        )
        dc1394? ( media-libs/libdc1394:2[>=2.1.0] )
        dirac? ( media-libs/schroedinger[>=1.0.10] )
        dts? ( media-libs/libdca[>=0.0.5] )
        dv? (
            media-libs/libavc1394[>=0.5.3]
            media-libs/libraw1394[>=2.0.1]
        )
        dvb? ( media-libs/libdvbpsi[>=1.2.0] )
        dvd? (
            media-libs/libdvdnav[>=4.9.0]
            media-libs/libdvdread[>=4.9.0]
        )
        flac? ( media-libs/flac:= )
        fluidsynth? ( media-sound/fluidsynth[>=1.1.2] )
        fontconfig? ( media-libs/fontconfig[>=2.11] )
        gnutls? ( dev-libs/gnutls[>=3.3.6] )
        gstreamer? ( media-plugins/gst-plugins-base:1.0 )
        h264? ( media-libs/x264:=[>=20180221] )
        hevc? ( media-libs/x265:= )
        jack? ( media-sound/jack-audio-connection-kit[>=0.120.1] )
        keyring? ( dev-libs/libsecret:1[>=0.18] )
        libnotify? (
            x11-libs/gtk+:3
            x11-libs/libnotify
        )
        libsamplerate? ( media-libs/libsamplerate )
        linsys? ( media-libs/zvbi[>=0.2.28] )
        live? ( media-libs/live[>=20111223] )
        matroska? (
            media-libs/libebml[>=1.3.6]
            media-libs/libmatroska[>=1.0.0]
        )
        mp2? ( media-libs/twolame )
        mp3? ( media-libs/libmad )
        mpc? ( media-libs/libmpcdec )
        mpeg2? ( media-libs/libmpeg2[>=0.3.2] )
        mtp? ( media-libs/libmtp[>=1.0.0] )
        nfs? ( net-fs/libnfs[>=1.10.0] )
        ogg? ( media-libs/libogg[>=1.0] )
        opus? ( media-libs/opus[>=1.0.3] )
        oss? ( sys-sound/oss )
        pulseaudio? ( media-sound/pulseaudio[>=1.0] )
        qt5? (
            x11-libs/qtbase:5[>=5.5.0][gui]
            x11-libs/qtsvg:5
            x11-libs/qtx11extras:5
        )
        samba? ( net-fs/samba )
        sftp? ( net-libs/libssh2 )
        shout? ( media-libs/libshout[>=2.1] )
        skins? (
            x11-libs/libXext
            x11-libs/libXinerama
            x11-libs/libXpm
        )
        sndio? ( sys-sound/sndio )
        soxr? ( media-libs/soxr[>=0.1.2] )
        speex? ( media-libs/speex[>=1.0.5] )
        svg? (
            gnome-desktop/librsvg:2[>=2.9.0]
            x11-libs/cairo[>=1.13.1]
        )
        taglib? ( media-libs/taglib[>=1.9] )
        teletext? ( media-libs/zvbi[>=0.2.28] )
        theora? ( media-libs/libtheora[>=1.0] )
        truetype? (
            media-libs/freetype:2
            x11-libs/harfbuzz
        )
        upnp? ( net-libs/libupnp )
        va? ( x11-libs/libva[>=1.6.0][X] )
        vdpau? ( x11-libs/libvdpau[>=0.6] )
        vorbis? (
            media-libs/libogg[>=1.1]
            media-libs/libvorbis[>=1.1]
        )
        vpx? ( media-libs/libvpx:=[>=1.5.0] )
        wayland? (
            sys-libs/wayland[>=1.5.91]
            sys-libs/wayland-protocols[>=1.4]
        )
        x262? ( media-libs/x262 )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd[>=142] )
    run:
        kwallet? ( kde-frameworks/kwallet:5 )
    suggestion:
        net-libs/libproxy:1 [[
            description = [ Proxy support via libproxy when accessing media via http ]
        ]]
"

if ever at_least 4.0.0_pre20230302 ; then
    MYOPTIONS+="
        nvenc [[ description = [ Enable NVIDIA hardware accelerated Decoder/Encoder (NVDEC/NVENC) API ] ]]
        ( platform: amd64 x86 )
    "
    DEPENDENCIES+="
        build:
            sys-devel/gettext[>=0.21]
            nvenc? ( media-libs/nv-codec-headers )
            platform:amd64? ( dev-lang/nasm )
            platform:x86? ( dev-lang/nasm )
        build+run:
            dev-libs/libglvnd
            x11-dri/libdrm[>=2.4.83]
            x11-dri/mesa
            x11-libs/libSM
            x11-libs/libxcb[>=1.8]
            x11-libs/libxkbcommon
            av1? ( media-libs/dav1d[>=0.5.0] )
            chromecast? ( dev-libs/protobuf:=[>=3.1.0] )
            dvd? (
                media-libs/libdvdnav[>=6.0.0]
                media-libs/libdvdread[>=6.0.0]
            )
            gnutls? ( dev-libs/gnutls[>=3.5.0] )
            gstreamer? (
                dev-libs/glib:2
                media-libs/gstreamer:1.0
            )
            pulseaudio? ( media-sound/pulseaudio[>=6.0] )
            qt5? (
                x11-libs/gtk+:3[>=3.20]
                x11-libs/qtbase:5[>=5.11.0][gui]
                x11-libs/qtdeclarative:5
                x11-libs/qtquickcontrols2:5
                x11-libs/qtwayland:5
            )
            shout? ( media-libs/libshout[>=2.4.3] )
            skins? ( x11-libs/libXcursor )
            upnp? ( net-libs/libupnp[>=1.8.3] )
            wayland? (
                sys-libs/wayland-protocols[>=1.5]
                x11-libs/libxkbcommon[wayland?]
            )
     "
else
    MYOPTIONS+="
        aalib [[ description = [ Support ASCII Art video output ] ]]
        sdl [[ description = [ Support image decoding with SDL_image ] ]]
        va [[
            description = [ Accelerate ffmpeg decoding by using the Video Acceleration API (DISABLE TO ALLOW FFMPEG >=5) ]
            requires = [ ffmpeg_abis: 4 ]
        ]]
    "
    DEPENDENCIES+="
        build+run:
            aalib? ( media-libs/aalib )
            sdl? ( media-libs/SDL_image:1[>=1.2.10] )
        run:
            qt5? (
                wayland? ( x11-libs/qtwayland:5 )
            )
    "
fi

vlc_src_prepare() {
    # TODO: report upstream, fix loading locales
    edo sed \
        -e 's:%s/../locale:/usr/share/locale:g' \
        -i src/modules/textdomain.c

    default
}

vlc_src_configure() {
    local myconf=()

    # -O/-f flags:
    #   vlc configure passes some -O/-f flags based on --enable-optimization and --enable-debug
    #   debug defaults to disable and optimization to enable

    # default disabled flags:
    #   altivec, neon (default enabled on arm)
    #   optimize-memory: optimization flags
    #   cprof, gprof: profiling support
    #   run-as-root: always disabled for security reasons
    #   coverage: see gcc(1) --coverage
    #   merge-ffmpeg: dunno
    #   realrtsp: proprietary extensions that Real applies to the open RTSP protocol
    #   tremor: dunno but needs -logg -lvorbisdec
    #   omxil: OpenMAX IL Video/Audio decoder
    #   x262: H262 encoding

    # default enabled flags:
    #   sout: stream output modules (makes it possible to trigger mux_ogg and shout)
    #   vlm: videolan manager (default enabled when sout is enabled)
    #   vcd: built-in VCD and CD-DA support (can make use of libcddb)
    #   screen: screen capture, doesn't seem to do anything on linux
    #   fb: linux framebuffer
    #   vlc: the vlc player

    # unwritten or not tested, disable to avoid automagic linking
    local disabled_params=(
        aom aribb25 aribsub bpg chromaprint d3d11va daala decklink directx dsm
        fdkaac fluidlite freerdp gme goom kate libplacebo lirc mfx mmal mod opencv
        osx-notifications pdb projectm shine sid smb2 sparkle spatialaudio srt telx tiger
        update-check vnc vsxu wasapi winstore-app
    )

    if ever at_least 4.0.0_pre20230302 ; then
        disabled_params+=(
            aribcaption basu davs2 debug ebur128 elogind medialibrary rav1e rist rnnoise
            vulkan
        )
    else
        disabled_params+=(
            crystalhd evas libtar tizen-audio
        )
    fi

    # alternative mp3 decoder to libmad
    disabled_params+=( mpg123 )

    for disabled_param in ${disabled_params[@]} ; do
        myconf+=( "--disable-${disabled_param}" )
    done

    export BUILDCC="$(exhost --build)-cc"

    option oss && append-flags -I/usr/$(exhost --target)/lib/oss/include

    # used in configure & m4/po.m4
    unset LINGUAS

    if option lua ; then
        export LUA_CFLAGS=$(${PKG_CONFIG} --cflags lua-$(lua_get_abi))
        export LUA_LIBS=$(${PKG_CONFIG} --libs lua-$(lua_get_abi))
        export LUAC=/usr/$(exhost --target)/bin/luac$(lua_get_abi)
    fi

    # Basic parameters
    myconf+=(
        --enable-alsa
        --enable-css
        --enable-dbus
        --enable-jpeg
        --enable-libxml2
        --enable-ncurses
        --enable-nls
        --enable-postproc
        --enable-udev
        --enable-xcb
        --with-x
        # depends on kde4 (kdelibs), which is going to die soon, so hard disable
        --without-kde-solid
        --without-libfuzzer
    )

    # Options
    myconf+=(
        $(option_enable a52)
        $(option_enable aac faad)
        $(option_enable archive)
        $(option_enable ass libass)
        $(option_enable av1 dav1d)
        $(option_enable avahi)
        $(option_enable bidi fribidi)
        $(option_enable bluray)
        $(option_enable caca)
        $(option_enable cddb libcddb)
        $(option_enable chromecast)
        $(option_enable chromecast microdns)
        $(option_enable crypt libgcrypt)
        $(option_enable dc1394)
        $(option_enable dirac schroedinger)
        $(option_enable dv dv1394)
        $(option_enable dts dca)
        $(option_enable dvb dvbpsi)
        $(option_enable dvd dvdnav)
        $(option_enable dvd dvdread)
        $(option_enable ffmpeg avcodec)
        $(option_enable ffmpeg avformat)
        $(option_enable ffmpeg swscale)
        $(option_enable flac)
        $(option_enable fluidsynth)
        $(option_enable fontconfig)
        $(option_enable gnutls)
        $(option_enable gstreamer gst-decode)
        $(option_enable h264 x264)
        $(option_enable h264 x26410b)
        $(option_enable hevc x265)
        $(option_enable jack)
        $(option_enable keyring secret)
        $(option_enable kwallet)
        $(option_enable libnotify notify)
        $(option_enable libsamplerate samplerate)
        $(option_enable linsys)
        $(option_enable live live555)
        $(option_enable lua)
        $(option_enable matroska)
        $(option_enable mp2 twolame)
        $(option_enable mp3 mad)
        $(option_enable mpc)
        $(option_enable mpeg2 libmpeg2)
        $(option_enable mtp)
        $(option_enable nfs)
        $(option_enable ogg)
        $(option_enable ogg oggspots)
        $(option_enable opus)
        $(option_enable oss)
        $(option_enable pulseaudio pulse)
        $(option_enable qt5 qt)
        $(option_enable samba smbclient)
        $(option_enable sftp)
        $(option_enable shout)
        $(option_enable skins skins2)
        $(option_enable sndio)
        $(option_enable soxr)
        $(option_enable speex)
        $(option_enable svg)
        $(option_enable svg svgdec)
        $(option_enable taglib)
        $(option_enable teletext zvbi)
        $(option_enable theora)
        $(option_enable truetype freetype)
        $(option_enable truetype harfbuzz)
        $(option_enable upnp)
        $(option_enable v4l v4l2)
        $(option_enable va libva)
        $(option_enable vdpau)
        $(option_enable vpx)
        $(option_enable vorbis)
        $(option_enable wayland)
    )

    if ever at_least 4.0.0_pre20230302 ; then
        myconf+=(
            --enable-gbm
            --enable-libdrm
            --without-wix
            $(option_enable nvenc nvdec)
        )
    else
        myconf+=(
            --enable-wma-fixed
            --enable-xvideo
            $(option_enable aalib aa)
            $(option_enable sdl sdl-image)
        )
    fi

    econf "${myconf[@]}"
}

vlc_src_install() {
    default

    edo cd doc
    dodoc -r skins

    # https://trac.videolan.org/vlc/ticket/19324
    if option lua ; then
        dosym /usr/share/vlc/lua/http /usr/$(exhost --target)/lib/vlc/lua/http
    fi
}

vlc_pkg_postinst() {
    if $(exhost --is-native -q ) ; then
        # refresh the plugin cache after installation
        if ever at_least 4.0.0_pre20230302 ; then
            edo /usr/$(exhost --target)/libexec/vlc/vlc-cache-gen \
                /usr/$(exhost --target)/lib/vlc/plugins
        else
            edo /usr/$(exhost --target)/lib/vlc/vlc-cache-gen \
                /usr/$(exhost --target)/lib/vlc/plugins
        fi
    fi

    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

vlc_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

