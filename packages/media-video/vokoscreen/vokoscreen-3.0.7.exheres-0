# Copyright 2016 Timo Gurr <tgurr@exherbo.org>
# Copyright 2020 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

MY_PN=vokoscreenNG

require github [ user=vkohaupt ] qmake [ slot=5 ]

SUMMARY="Easy to use screencast creator"
DESCRIPTION="
vokoscreenNG is an easy to use screencast creator to record educational videos, live recordings of
browser, installation, videoconferences, etc.
"
HOMEPAGE+=" http://linuxecke.volkoh.de/${PN}/${PN}.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
# XXX(kapsh): options only to pull run deps in, encoders can actually be auto-picked later
MYOPTIONS="
    ( aac flac mp3 vorbis ) [[
        *description = [ Audio output format ]
    ]]
    ( openh264 x264 ) [[
        *description = [ Video output format ]
    ]]
    v4l [[
        description = [ Add camera stream to screencasts ]
    ]]
"

DEPENDENCIES="
    build:
        x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
        virtual/pkg-config
    build+run:
        media-libs/gstreamer:1.0[>=1.12.5]
        media-sound/pulseaudio
        x11-libs/libX11
        x11-libs/qtbase:5[gui]
        x11-libs/qtx11extras:5
        x11-libs/qtmultimedia:5[gstreamer] [[
            note = [ Built-in player and camera stream capture ] ]]
    run:
        media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opus] [[
            note = [ Opus audio output ] ]]
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:vpx] [[
            note = [ VP8 video output ] ]]
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:X][gstreamer_plugins:pulseaudio] [[
            note = [ Basic video/audio capture and containers formats support ] ]]
        aac? ( media-plugins/gst-plugins-bad:1.0[gstreamer_plugins:vo-aacenc] )
        flac? ( media-plugins/gst-plugins-good:1.0[gstreamer_plugins:flac] )
        mp3? ( media-plugins/gst-plugins-good:1.0[gstreamer_plugins:lame] )
        openh264? ( media-plugins/gst-plugins-bad:1.0[gstreamer_plugins:openh264] )
        v4l? ( media-plugins/gst-plugins-good:1.0[gstreamer_plugins:v4l] )
        vorbis? ( media-plugins/gst-plugins-base:1.0[gstreamer_plugins:vorbis] )
        x264? ( media-plugins/gst-plugins-ugly:1.0[gstreamer_plugins:h264] )
"

EQMAKE_SOURCES=( "src/${MY_PN}.pro" )

src_install() {
    # manual install: https://github.com/vkohaupt/vokoscreenNG/issues/75

    dobin ${MY_PN}

    insinto /usr/share/applications
    doins src/applications/${MY_PN}.desktop

    insinto /usr/share/pixmaps
    doins src/applications/${MY_PN}.png
}

