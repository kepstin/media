# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hpjansson release=${PV} suffix=tar.xz ]

SUMMARY="Terminal graphics for the 21st century"
DESCRIPTION="
Chafa is a command-line utility that converts all kinds of images, including
animated GIFs, into sixel or ANSI/Unicode character output that can be
displayed in a terminal.
It is highly configurable, with support for alpha transparency and multiple
color modes and color spaces, combining selectable ranges of Unicode
characters to produce the desired output.
The core functionality is provided by a C library with a public,
well-documented API."

HOMEPAGE+=" https://hpjansson.org/chafa/"
UPSTREAM_DOCUMENTATION="https://hpjansson.org/chafa/ref/"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk-doc [[ description = [ Rebuild API documentation ] ]]
    tools   [[ description = [ Build and install a command-line tool ] ]]
    svg
    tiff
    webp    [[ description = [ Support for the WebP image file format ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( svg tiff webp ) [[ *requires = tools ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.26]
        tools? (
            media-libs/freetype:2[>=2.0.0]
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
            svg? ( gnome-desktop/librsvg:2 )
            tiff? ( media-libs/tiff:= )
            webp? ( media-libs/libwebp:= )
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-man
    --disable-static
    --with-jpeg
    --without-imagemagick
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( gtk-doc )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    svg
    tiff
    tools
    webp
)


